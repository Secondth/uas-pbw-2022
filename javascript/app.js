var daftarPesanan = [];

document.addEventListener('DOMContentLoaded', function() {
  var form = document.querySelector('#form-pesanan');
  var table = document.querySelector('#tabel-pesanan');

  form.addEventListener('submit', function(event) {
    event.preventDefault();

    console.log(daftarPesanan);

  var nomorMeja = form.elements['nomor_meja'].value;
  var menuPesanan = form.elements['menu_pesanan'].value;
  var jumlahPesanan = form.elements['jumlah_pesanan'].value;
    
daftarPesanan.push({
    nomorMeja: nomorMeja,
    menuPesanan: menuPesanan,
    jumlahPesanan: jumlahPesanan
  });

  var row = document.createElement('tr');
  row.innerHTML = '<td>' + nomorMeja + '</td>' +
    '<td>' + menuPesanan + '</td>' +
    '<td>' + jumlahPesanan + '</td>' +
    '<td>' +
    '<button class="edit-btn">Edit</button>' +
    '<button class="delete-btn">Delete</button>' +
    '</td>';

  table.appendChild(row);

  form.elements['nomor_meja'].value = '';
  form.elements['menu_pesanan'].value = '';
  form.elements['jumlah_pesanan'].value = '';
  });

  table.addEventListener('click', function(event) {
    if (event.target.classList.contains('delete-btn')) {
      event.target.parentNode.parentNode.remove();

  var row = event.target.parentNode.parentNode;
  var nomorMeja = row.querySelector('td:nth-child(1)').textContent;
  var menuPesanan = row.querySelector('td:nth-child(2)').textContent;
  var jumlahPesanan = row.querySelector('td:nth-child(3)').textContent;

  row.remove();
  
  var index = -1;
  for (var i = 0; i < daftarPesanan.length; i++) {
    if (daftarPesanan[i].nomorMeja.toString() == nomorMeja &&
        daftarPesanan[i].menuPesanan.toString() == menuPesanan &&
        daftarPesanan[i].jumlahPesanan.toString() == jumlahPesanan) {
      index = i;
      break;
    }
  }

  if (index !== -1) {
    daftarPesanan.splice(index, 1);
  }

    }
  });

  table.addEventListener('click', function(event) {
    if (event.target.classList.contains('edit-btn')) {
      var row = event.target.parentNode.parentNode;
      var nomorMeja = row.querySelector('td:nth-child(1)').textContent;
      var menuPesanan = row.querySelector('td:nth-child(2)').textContent;
      var jumlahPesanan = row.querySelector('td:nth-child(3)').textContent;

      form.elements['nomor_meja'].value = nomorMeja;
      form.elements['menu_pesanan'].value = menuPesanan;
      form.elements['jumlah_pesanan'].value = jumlahPesanan;

      row.remove();

      var index = -1;
      for (var i = 0; i < daftarPesanan.length; i++) {
        if (daftarPesanan[i].nomorMeja.toString() == nomorMeja &&
        daftarPesanan[i].menuPesanan.toString() == menuPesanan &&
        daftarPesanan[i].jumlahPesanan.toString() == jumlahPesanan) {
          index = i;
          break;
        }
      }

      if (index !== -1) {
        daftarPesanan.splice(index, 1);
      }

      var index = -1;
      for (var i = 0; i < daftarPesanan.length; i++) {
        if (daftarPesanan[i].nomorMeja.toString() == nomorMeja &&
        daftarPesanan[i].menuPesanan.toString() == menuPesanan &&
        daftarPesanan[i].jumlahPesanan.toString() == jumlahPesanan) {
          index = i;
          break;
        }
      }

      if (index !== -1) {
        daftarPesanan[index] = {
          nomorMeja: form.elements['nomor_meja'].value,
          menuPesanan: form.elements['menu_pesanan'].value,
          jumlahPesanan: form.elements['jumlah_pesanan'].value
        };
      }  
    }
  });
});
